#include <graphics3d/JsonParser.h>
#include <graphics3d/SceneNode.h>
#include <graphics3d/BoundingBox.h>
#include <graphics3d/Model3d.h>
// #include <SDL2/SDL_log.h>
#include <json-c/json.h>
#include <memory>

constexpr char KEY_MODELS_ROOT_PATH[] { "models_root_path" };
constexpr char KEY_CHILDREN[] { "children" };
constexpr char KEY_NAME[] { "name" };
constexpr char KEY_PATH[] { "path" };
constexpr char KEY_SCALE[] { "scale" };
constexpr char KEY_TRANSLATION[] { "translation" };
constexpr char KEY_ROT_EULER[] { "rot_euler" };
constexpr char KEY_ROT_QUAT[] { "rot_quat" };

JsonParser::JsonParser(SceneNode* root)
    : m_root(root)
{
}

bool JsonParser::Load(const std::string& fileName)
{
#if defined(_WIN32)
    std::FILE* fp;
    if (fopen_s(&fp, fileName.c_str(), "rb") != 0)
    {
        // SDL_Log("Failed to open world json file: %s\n", fileName.c_str());
        return false;
    }
#else
    std::FILE* fp = std::fopen(fileName.c_str(), "rb");
    if (!fp)
    {
        // SDL_Log("Failed to open world json file: %s\n", fileName.c_str());
        return false;
    }
#endif

    if (std::fseek(fp, 0, SEEK_END) != 0)
    {
        // SDL_Log("Failed to open world json file: %s\n", fileName.c_str());
        std::fclose(fp);
        return false;
    }

    long size = ftell(fp);
    auto jsonFileBuff = std::make_unique<char[]>(size);

    std::rewind(fp);
    size_t read = std::fread(jsonFileBuff.get(), sizeof(char), static_cast<size_t>(size), fp);
    if (read != static_cast<size_t>(size) || std::ferror(fp) != 0)
    {
        // SDL_Log("Failed to read world json file: %s\n", fileName.c_str());
        std::fclose(fp);
        return false;
    }
    std::fclose(fp);

    json_object *jsonRoot = json_tokener_parse(jsonFileBuff.get());
    if (!jsonRoot)
    {
        // SDL_Log("Parse failure on world json file: %s\n", fileName.c_str());
        return false;
    }

    json_object *jsonModelsRootPath = nullptr;
    bool ok = json_object_object_get_ex(jsonRoot, KEY_MODELS_ROOT_PATH, &jsonModelsRootPath);
    ok = ok && jsonModelsRootPath;
    ok = ok && json_object_get_type(jsonModelsRootPath) == json_type_string;
    if (!ok)
    {
        // SDL_Log("Parse failure on world json file: %s. No models root path.\n", fileName.c_str());
        json_object_put(jsonRoot);
        return false;
    }

    m_modelsRootPath = json_object_get_string(jsonModelsRootPath);

    json_object *jsonRootNode = nullptr;
    if (!(json_object_object_get_ex(jsonRoot, KEY_CHILDREN, &jsonRootNode) &&
          json_object_get_type(jsonRootNode) == json_type_array))
    {
        // SDL_Log("Parse failure on world json file: %s. No nodes.\n", fileName.c_str());
        json_object_put(jsonRoot);
        return false;
    }

    LoadNode(jsonRootNode, m_root);
    json_object_put(jsonRoot);
    return ok;
}

void JsonParser::LoadNode(json_object* rootJson, SceneNode* rootSceneNode)
{
    assert(rootSceneNode);

    const size_t lenChildren = json_object_array_length(rootJson);
    rootSceneNode->Reserve(lenChildren);

    for (size_t i = 0; i < lenChildren; ++i)
    {
        json_object *childJson = json_object_array_get_idx(rootJson, i);

        std::string name;
        std::string path;
        json_object *value = nullptr;

        // mandatory keys
        json_bool ok = json_object_object_get_ex(childJson, KEY_NAME, &value);
        ok = ok && json_object_get_type(value) == json_type_string;
        if (ok)
        {
            name = json_object_get_string(value);
        }
        ok = ok && json_object_object_get_ex(childJson, KEY_PATH, &value);
        ok = ok && json_object_get_type(value) == json_type_string;
        if (ok)
        {
            path = json_object_get_string(value);
        }

        if (!ok)
        {
            // SDL_Log("Skipping node, missing or invalid mandatory keys\n");
            continue;
        }

        // optional keys
        float scale = 1.0f;
        glm::vec3 translation(0.0f);
        glm::vec3 rot_euler(0.0f);
        float rot_quat_angle(0.0f);
        glm::vec3 rot_quat_axis(1.0f, 0.0f, 0.0f);
        int nodeFlags = JsonSceneNodeFlags::HAS_NOTHING;

        if (json_object_object_get_ex(childJson, KEY_SCALE, &value) &&
            json_object_get_type(value) == json_type_double)
        {
            scale = static_cast<float>(json_object_get_double(value));
        }

        if (json_object_object_get_ex(childJson, KEY_TRANSLATION, &value) &&
            json_object_get_type(value) == json_type_array &&
            json_object_array_length(value) == 3)
        {
            for (int j = 0; j < 3; ++j)
            {
                json_object *elem = json_object_array_get_idx(value, j);
                if (json_object_get_type(elem) == json_type_double)
                {
                    translation[j] = static_cast<float>(json_object_get_double(elem));
                }
            }
            nodeFlags |= JsonSceneNodeFlags::HAS_TRANSLATION;
        }

        if (json_object_object_get_ex(childJson, KEY_ROT_EULER, &value) &&
            json_object_get_type(value) == json_type_array &&
            json_object_array_length(value) == 3)
        {
            for (int j = 0; j < 3; ++j)
            {
                json_object *elem = json_object_array_get_idx(value, j);
                if (json_object_get_type(elem) == json_type_double)
                {
                    rot_euler[j] = static_cast<float>(json_object_get_double(elem));
                }
            }
            nodeFlags |= JsonSceneNodeFlags::HAS_ROTATION_EULER;
        }
        else if (json_object_object_get_ex(childJson, KEY_ROT_QUAT, &value) &&
                 json_object_get_type(value) == json_type_array &&
                 json_object_array_length(value) == 4)
        {
            json_object *elem = json_object_array_get_idx(value, 0);
            if (json_object_get_type(elem) == json_type_double)
            {
                rot_quat_angle = static_cast<float>(json_object_get_double(elem));
            }
            for (int j = 1; j < 4; ++j)
            {
                json_object *elem = json_object_array_get_idx(value, j);
                if (json_object_get_type(elem) == json_type_double)
                {
                    rot_quat_axis[j - 1] = static_cast<float>(json_object_get_double(elem));
                }
            }
            nodeFlags |= JsonSceneNodeFlags::HAS_ROTATION_QUAT;
        }

        std::shared_ptr<Model3d> pModel3d = m_loader.Load(name, m_modelsRootPath + path, scale);

        std::unique_ptr<SceneNode> node = std::make_unique<SceneNode>(pModel3d);
        if (pModel3d)
        {
            node->SetOrigin(pModel3d->GetBBox().center());
        }

        if (nodeFlags & JsonSceneNodeFlags::HAS_TRANSLATION)
        {
            node->SetTranslation(translation);
        }
        if (nodeFlags & JsonSceneNodeFlags::HAS_ROTATION_EULER)
        {
            node->SetRotation(rot_euler.x, rot_euler.y, rot_euler.z);
        }
        else if (nodeFlags & JsonSceneNodeFlags::HAS_ROTATION_QUAT)
        {
            node->SetRotation(rot_quat_angle, rot_quat_axis);
        }

        // load children of this node if they exist
        if (json_object_object_get_ex(childJson, KEY_CHILDREN, &value) &&
            json_object_get_type(value) == json_type_array)
        {
            LoadNode(value, node.get());
        }

        rootSceneNode->AddChild(std::move(node)); // take ownership
    } // end of node children loop
}
