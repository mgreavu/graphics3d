#include <graphics3d/ModelLoader.h>
#include <graphics3d/Model3d.h>
#include <graphics3d/Texture2D.h>
#include <graphics3d/Material.h>
#include <assimp/Importer.hpp>
#include <assimp/matrix4x4.h>
#include <assimp/scene.h>       // Output data structure
#include <assimp/postprocess.h> // Post processing flags
#include <assimp/color4.h>
// #include <SDL2/SDL_log.h>

#include <glm/gtc/matrix_transform.hpp>
#include <algorithm>
#include <utility>

// OpenGL and glm use column major, Assimp uses row major
static glm::mat4 AssimpMat4ToGlmMat4(const aiMatrix4x4& from)
{
    glm::mat4 to;
    to[0][0] = from.a1; to[1][0] = from.a2; to[2][0] = from.a3; to[3][0] = from.a4;
    to[0][1] = from.b1; to[1][1] = from.b2; to[2][1] = from.b3; to[3][1] = from.b4;
    to[0][2] = from.c1; to[1][2] = from.c2; to[2][2] = from.c3; to[3][2] = from.c4;
    to[0][3] = from.d1; to[1][3] = from.d2; to[2][3] = from.d3; to[3][3] = from.d4;
    return to;
}

ModelLoader::ModelLoader()
    : m_meshIndex(0)
    , m_scaleTransform(1.0f)
    , m_postProcessFlags(aiProcess_Triangulate |
                         aiProcess_FindInvalidData |
                         aiProcess_GenSmoothNormals |
                         aiProcess_JoinIdenticalVertices |
                         aiProcess_GenUVCoords |
                         aiProcess_FixInfacingNormals |
                         aiProcess_OptimizeMeshes |
                         aiProcess_FindDegenerates |
                         aiProcess_SortByPType)
{
}

ModelLoader::~ModelLoader()
{
    Clear();
}

void ModelLoader::Clear()
{
    m_meshIndex = 0;
    m_scaleTransform = glm::mat4(1.0f);
    m_mapMat.clear();
    m_mapTex.clear();
    // get from a valid undefined state to a valid defined state
    // following the std::move() call in Load()
    m_meshes.clear();
    m_meshes.shrink_to_fit();
}

std::shared_ptr<Model3d> ModelLoader::Load(const std::string& name, const std::string& fileName, float scale)
{
    Assimp::Importer Importer;
    Importer.SetPropertyFloat(AI_CONFIG_PP_GSN_MAX_SMOOTHING_ANGLE, 80.f);
    Importer.SetPropertyInteger(AI_CONFIG_PP_SBP_REMOVE, aiPrimitiveType_LINE | aiPrimitiveType_POINT);

    const aiScene* pScene = Importer.ReadFile(fileName.c_str(), m_postProcessFlags);
    if (!pScene)
    {
        // SDL_Log("Load() --> Error parsing '%s': '%s'\n", fileName.c_str(), Importer.GetErrorString());
        return std::shared_ptr<Model3d>();
    }

    // SDL_Log("Load() --> Name: '%s', meshes: %u, materials: %u\n", name.c_str(), pScene->mNumMeshes, pScene->mNumMaterials);

    m_scaleTransform = glm::scale(glm::mat4(1.0f), glm::vec3(scale));

    m_meshes.resize(pScene->mNumMeshes);

    const aiNode* pNode = pScene->mRootNode;
    InitNode(pNode, pScene, AssimpMat4ToGlmMat4(pNode->mTransformation));
    InitMaterials(pScene, fileName);

    // sort by alpha
    std::sort(m_meshes.begin(), m_meshes.end(),
        [](const Mesh& m1, const Mesh& m2) -> bool
    {
        return (!(m1.m_pMaterial->m_pTexDiffuse && m1.m_pMaterial->m_pTexDiffuse->HasAlpha()) &&
                  m2.m_pMaterial->m_pTexDiffuse && m2.m_pMaterial->m_pTexDiffuse->HasAlpha()
                );
    });

    const auto bbox = MakeBBox(); // RVO
    assert(bbox.valid());
    auto spModel3d = std::make_shared<Model3d>(name, bbox, std::move(m_meshes));
    Clear();
    return spModel3d;
}

void ModelLoader::InitNode(const aiNode* pNode, const aiScene* pScene, const glm::mat4& parentTransform)
{
    if (!pNode || !pScene)
    {
        return;
    }
    const glm::mat4 transform = parentTransform * AssimpMat4ToGlmMat4(pNode->mTransformation);

    for (unsigned int i = 0; i < pNode->mNumMeshes; ++i)
    {
        const aiMesh* paiMesh = pScene->mMeshes[pNode->mMeshes[i]]; // get aiNode's aiMesh
        if (paiMesh)
        {
            InitMesh(paiMesh, transform);
            m_meshIndex++;
        }
    }

    for (unsigned int i = 0; i < pNode->mNumChildren; ++i)
    {
        InitNode(pNode->mChildren[i], pScene, transform);
    }
}

void ModelLoader::InitMesh(const aiMesh* paiMesh, const glm::mat4& transform)
{
    assert(m_meshIndex < m_meshes.size());
    assert(paiMesh->HasPositions());
    assert(paiMesh->HasNormals());
    // SDL_Log("InitMesh() --> name: '%s', hasTangentsAndBitangents: %s\n", paiMesh->mName.C_Str(), paiMesh->HasTangentsAndBitangents() ? "true" : "false");

    const aiVector3D aiZero3D(0.0f, 0.0f, 0.0f);
    glm::vec3 min(FLT_MAX), max(-FLT_MAX);

    // material
    m_meshes[m_meshIndex].m_MaterialIndex = paiMesh->mMaterialIndex;

    m_meshes[m_meshIndex].m_vertices.reserve(paiMesh->mNumVertices);
    for (unsigned int i = 0; i < paiMesh->mNumVertices; ++i)
    {
        // position
        const aiVector3D* paiPosition = &(paiMesh->mVertices[i]);
        const glm::vec4 origPos(paiPosition->x, paiPosition->y, paiPosition->z, 1.0f);
        const glm::vec4 scaledPos = m_scaleTransform * origPos;
        const glm::vec3 position = glm::vec3(transform * scaledPos);

        if (position.x < min.x) { min.x = position.x; }
        if (position.y < min.y) { min.y = position.y; }
        if (position.z < min.z) { min.z = position.z; }

        if (position.x > max.x) { max.x = position.x; }
        if (position.y > max.y) { max.y = position.y; }
        if (position.z > max.z) { max.z = position.z; }

        // normal
        const aiVector3D* paiNormal = &(paiMesh->mNormals[i]);
        const glm::vec3 normal = glm::normalize(glm::vec3(transform * glm::vec4(paiNormal->x, paiNormal->y, paiNormal->z, 1.0f)));

        // texture coordinate
        const aiVector3D* paiTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &aiZero3D;

        m_meshes[m_meshIndex].m_vertices.emplace_back(position, glm::vec2(paiTexCoord->x, paiTexCoord->y), normal);
    }

    // bounding box
    m_meshes[m_meshIndex].m_bbox.set(min, max);
    assert(m_meshes[m_meshIndex].m_bbox.valid());

    // indices
    m_meshes[m_meshIndex].m_indices.reserve(paiMesh->mNumFaces * 3);
    for (unsigned int i = 0; i < paiMesh->mNumFaces; ++i)
    {
        const aiFace& face = paiMesh->mFaces[i];
        assert(face.mNumIndices == 3); // triangles

        m_meshes[m_meshIndex].m_indices.emplace_back(face.mIndices[0]);
        m_meshes[m_meshIndex].m_indices.emplace_back(face.mIndices[1]);
        m_meshes[m_meshIndex].m_indices.emplace_back(face.mIndices[2]);
    }
}

void ModelLoader::InitMaterials(const aiScene* pScene, const std::string& fileName)
{
    assert(pScene->HasMaterials());

    // Extract the directory part from the file name
    std::string folderPath;
    size_t slashIndex = fileName.find_last_of("/");

    if (slashIndex == std::string::npos)
    {
        folderPath = ".";
    }
    else if (slashIndex == 0)
    {
        folderPath = "/";
    }
    else
    {
        folderPath = fileName.substr(0, slashIndex);
    }

    const auto& textures = m_mapTex;

    for (unsigned int idxMat = 0; idxMat < pScene->mNumMaterials; ++idxMat)
    {
        const aiMaterial* paiMaterial = pScene->mMaterials[idxMat];

        auto pMaterial = std::make_shared<Material>();

        aiString name;
        if (paiMaterial->Get(AI_MATKEY_NAME, name) == AI_SUCCESS)
        {
            pMaterial->m_name = name.C_Str();
        }

        aiColor4D color;
        if (aiGetMaterialColor(paiMaterial, AI_MATKEY_COLOR_AMBIENT, &color) == AI_SUCCESS)
        {
            pMaterial->m_Ka[0] = color.r;
            pMaterial->m_Ka[1] = color.g;
            pMaterial->m_Ka[2] = color.b;
            pMaterial->m_Ka[3] = color.a;
        }
        if (aiGetMaterialColor(paiMaterial, AI_MATKEY_COLOR_DIFFUSE, &color) == AI_SUCCESS)
        {
            pMaterial->m_Kd[0] = color.r;
            pMaterial->m_Kd[1] = color.g;
            pMaterial->m_Kd[2] = color.b;
            pMaterial->m_Kd[3] = color.a;
        }
        if (aiGetMaterialColor(paiMaterial, AI_MATKEY_COLOR_SPECULAR, &color) == AI_SUCCESS)
        {
            pMaterial->m_Ks[0] = color.r;
            pMaterial->m_Ks[1] = color.g;
            pMaterial->m_Ks[2] = color.b;
            pMaterial->m_Ks[3] = color.a;
        }

        float assimpValue = 0;
        if (paiMaterial->Get(AI_MATKEY_SHININESS, assimpValue) == AI_SUCCESS)
        {
            pMaterial->m_shininess = assimpValue;
//          at some point in Assimp's past you would empirically do this: pMat->m_shininess = value / 4.0f;
        }

        if (paiMaterial->Get(AI_MATKEY_SHININESS_STRENGTH, assimpValue) == AI_SUCCESS)
        {
            pMaterial->m_shininess = pMaterial->m_shininess * assimpValue;
            if (pMaterial->m_shininess >= 128.0f) // 128 is the maximum exponent as per the Gl spec
            {
                pMaterial->m_shininess = 128.0f;
            }
        }

        if (paiMaterial->Get(AI_MATKEY_OPACITY, assimpValue) == AI_SUCCESS)
        {
            pMaterial->m_opacity = assimpValue;
        }

        if (paiMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0)
        {
            aiString aiTextureFileName;
            if (paiMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &aiTextureFileName, nullptr, nullptr, nullptr, nullptr, nullptr) == AI_SUCCESS)
            {
                const std::string texturePath = folderPath + "/" + aiTextureFileName.data;
                auto it = textures.find(texturePath);
                if (it == m_mapTex.end())
                {
                    auto pTexture = std::make_shared<Texture2D>();
                    if (pTexture->Load(texturePath))
                    {
                        pMaterial->m_pTexDiffuse = pTexture;
                        m_mapTex.emplace(texturePath, pTexture);
                    }
                }
                else // already allocated
                {
                    pMaterial->m_pTexDiffuse = (*it).second;
                }
            }
        }

        if (paiMaterial->GetTextureCount(aiTextureType_SPECULAR) > 0)
        {
            aiString aiTextureFileName;
            if (paiMaterial->GetTexture(aiTextureType_SPECULAR, 0, &aiTextureFileName, nullptr, nullptr, nullptr, nullptr, nullptr) == AI_SUCCESS)
            {
                const std::string texturePath = folderPath + "/" + aiTextureFileName.data;
                auto it = textures.find(texturePath);
                if (it == m_mapTex.end())
                {
                    auto pTexture = std::make_shared<Texture2D>();
                    if (pTexture->Load(texturePath))
                    {
                        pMaterial->m_pTexSpecular = pTexture;
                        m_mapTex.emplace(texturePath, pTexture);
                    }
                }
                else // already allocated
                {
                    pMaterial->m_pTexSpecular = (*it).second;
                }
            }
        }

        if (paiMaterial->GetTextureCount(aiTextureType_NORMALS) > 0)
        {
            aiString aiTextureFileName;
            if (paiMaterial->GetTexture(aiTextureType_NORMALS, 0, &aiTextureFileName, nullptr, nullptr, nullptr, nullptr, nullptr) == AI_SUCCESS)
            {
                const std::string texturePath = folderPath + "/" + aiTextureFileName.data;
                auto it = textures.find(texturePath);
                if (it == m_mapTex.end())
                {
                    auto pTexture = std::make_shared<Texture2D>();
                    if (pTexture->Load(texturePath))
                    {
                        pMaterial->m_pTexNormals = pTexture;
                        m_mapTex.emplace(texturePath, pTexture);
                    }
                }
                else // already allocated
                {
                    pMaterial->m_pTexNormals = (*it).second;
                }
            }
        }

        m_mapMat.emplace(idxMat, pMaterial);
    }   // end materials loop

    // set mesh material
    for (auto& mesh : m_meshes)
    {
        std::map<unsigned int, std::shared_ptr<Material>>::const_iterator it = m_mapMat.find(mesh.m_MaterialIndex);
        if (it != m_mapMat.end())
        {
            mesh.m_pMaterial = it->second;
        }
    }
}

BoundingBox ModelLoader::MakeBBox() const
{
    // std::min_element(), std::max_element() = two traversals
    glm::vec3 min(FLT_MAX), max(-FLT_MAX);

    const auto& meshes = m_meshes;
    for (auto& mesh : meshes)
    {
        if (mesh.m_bbox._min.x < min.x) { min.x = mesh.m_bbox._min.x; }
        if (mesh.m_bbox._min.y < min.y) { min.y = mesh.m_bbox._min.y; }
        if (mesh.m_bbox._min.z < min.z) { min.z = mesh.m_bbox._min.z; }

        if (mesh.m_bbox._max.x > max.x) { max.x = mesh.m_bbox._max.x; }
        if (mesh.m_bbox._max.y > max.y) { max.y = mesh.m_bbox._max.y; }
        if (mesh.m_bbox._max.z > max.z) { max.z = mesh.m_bbox._max.z; }
    }
    return BoundingBox(min, max);
}
