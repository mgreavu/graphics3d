#include <graphics3d/TextBillboardProgram.h>

#include <glm/gtc/type_ptr.hpp>

bool TextBillboardProgram::Init()
{
    bool ok = GlslProgram::Init();
    ok = ok && AddShader(GL_VERTEX_SHADER, "shaders/billboard_ui.vs");

    glBindAttribLocation(m_shaderProg, 0, "PosCoord");
    glBindAttribLocation(m_shaderProg, 1, "TexCoord");

    ok = ok && AddShader(GL_FRAGMENT_SHADER, "shaders/billboard_ui.fs");
    ok = ok && Finalize();

    if (ok)
    {
        m_locTopLeft = GetUniformLocation("gTopLeft");
        m_locScale = GetUniformLocation("gScale");
        m_locViewProjMatrix = GetUniformLocation("gViewProj");
        m_locSampler = GetUniformLocation("gSampler");

        if (m_locViewProjMatrix == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locTopLeft == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locScale == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locSampler == static_cast<GLint>(INVALID_UNIFORM_LOCATION))
        {
            ok = false;
        }

    }
    return ok;
}

void TextBillboardProgram::SetViewProjMatrix(const glm::mat4& viewProj)
{
    glUniformMatrix4fv(m_locViewProjMatrix, 1, GL_FALSE, glm::value_ptr(viewProj));
}

void TextBillboardProgram::SetBillboardTopLeftCorner(const glm::vec3& vec)
{
    glUniform3f(m_locTopLeft, vec.x, vec.y, vec.z);
}

void TextBillboardProgram::SetBillboardScale(const glm::vec2& vec)
{
    glUniform2f(m_locScale, vec.x, vec.y);
}

void TextBillboardProgram::SetTextureUnit(GLint textureUnit)
{
    glUniform1i(m_locSampler, textureUnit);
}
