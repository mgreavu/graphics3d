#include <graphics3d/TextBillboard.h>
#include <graphics3d/Texture2D.h>
// #include <SDL2/SDL_log.h>
#include <glm/gtc/matrix_transform.hpp>

// glyphs texture atlas description of lucida_console_256x128_font_16_cell_16x16.png
static constexpr unsigned int ATLAS_COLS = 16; // columns (glyphs per row)
static constexpr unsigned int ATLAS_ROWS = 8;  // rows
static constexpr unsigned int ATLAS_GLYPH_WIDTH = 16; // pixels
static constexpr unsigned int ATLAS_GLYPH_HEIGHT = 16; // pixels

static constexpr unsigned int MAX_GLYPHS_PER_LINE = 128;

static bool CharToColRow(char ch, unsigned int& col, unsigned int& row)
{
    if (ch >= 32)
    {
        row = (ATLAS_ROWS - 1) - ((ch - 32) / ATLAS_COLS);
        col = (ch - 32) % ATLAS_COLS;
        return true;
    }
    return false;
}

TextBillboard::TextBillboard()
    : m_texAtlas(std::make_unique<Texture2D>())
    , m_texId(0)
{
}

TextBillboard::~TextBillboard()
{
    if (m_texAtlas->IsReady())
    {
        glDeleteTextures(1, &m_texId);
        m_texAtlas->SetReady(false);
    }

    for(auto& line : m_lines)
    {
        glDeleteBuffers(1, &line.second.m_vboId);
    }
}

void TextBillboard::InsertLine(int lineNo, const std::string& text)
{
    // NDC visible coordinates range [-1, 1] on all axes, meaning the visible width is 2
    constexpr float NDC_QUAD_SIDE = 2.0f;

    auto it = m_lines.find(lineNo);
    if (it == m_lines.end())
    {
        TextLine line(text);

        glGenBuffers(1, &line.m_vboId);
        glBindBuffer(GL_ARRAY_BUFFER, line.m_vboId);
        glBufferData(GL_ARRAY_BUFFER,
                     static_cast<GLsizeiptr>(sizeof(Vertex) * MAX_GLYPHS_PER_LINE * 6), // size
                     NULL, // no data to be copied
                     GL_DYNAMIC_DRAW); // the data store contents will be modified repeatedly and used many times

        m_lines.emplace(lineNo, std::move(line));

        it = m_lines.find(lineNo); // get iterator to this new line
    }
    else
    {
        it->second.m_vertices.clear();
    }

    it->second.m_vertices.reserve(6 * text.length());

    for (unsigned int i = 0; i < text.length(); ++i)
    {
        unsigned int col = 0, row = 0;
        CharToColRow(text[i], col, row);

        // the first triangle of the quad
        it->second.m_vertices.emplace_back(glm::vec3(i * NDC_QUAD_SIDE, -(lineNo + 1) * NDC_QUAD_SIDE, 0.0f),
                                           glm::vec2(static_cast<float>(col) / ATLAS_COLS, static_cast<float>(row) / ATLAS_ROWS));
        it->second.m_vertices.emplace_back(glm::vec3((i + 1) * NDC_QUAD_SIDE, -(lineNo + 1) * NDC_QUAD_SIDE, 0.0f),
                                           glm::vec2(static_cast<float>(col + 1) / ATLAS_COLS, static_cast<float>(row) / ATLAS_ROWS));
        it->second.m_vertices.emplace_back(glm::vec3(i * NDC_QUAD_SIDE, -lineNo * NDC_QUAD_SIDE, 0.0f),
                                           glm::vec2(static_cast<float>(col) / ATLAS_COLS, static_cast<float>(row + 1) / ATLAS_ROWS));

        // the second triangle of the quad
        it->second.m_vertices.emplace_back(glm::vec3((i + 1) * NDC_QUAD_SIDE, -(lineNo + 1) * NDC_QUAD_SIDE, 0.0f),
                                           glm::vec2(static_cast<float>(col + 1) / ATLAS_COLS, static_cast<float>(row) / ATLAS_ROWS));
        it->second.m_vertices.emplace_back(glm::vec3((i + 1) * NDC_QUAD_SIDE, -lineNo * NDC_QUAD_SIDE, 0.0f),
                                           glm::vec2(static_cast<float>(col + 1) / ATLAS_COLS, static_cast<float>(row + 1) / ATLAS_ROWS));
        it->second.m_vertices.emplace_back(glm::vec3(i * NDC_QUAD_SIDE, -lineNo * NDC_QUAD_SIDE, 0.0f),
                                           glm::vec2(static_cast<float>(col) / ATLAS_COLS, static_cast<float>(row + 1) / ATLAS_ROWS));
    }

    glBindBuffer(GL_ARRAY_BUFFER, it->second.m_vboId);
    glBufferSubData(GL_ARRAY_BUFFER,
                    0, // offset
                    static_cast<GLsizeiptr>(sizeof(Vertex) * it->second.m_vertices.size()), // size
                    reinterpret_cast<GLvoid*>(&it->second.m_vertices[0])); // data pointer
}

bool TextBillboard::Load()
{
    if (!m_texAtlas->Load("data/fonts/lucida_console_256x128_font_16_cell_16x16.png"))
    {
        // SDL_Log("Texture atlas load failure\n");
        return false;
    }

    if (!m_program.Init())
    {
        // SDL_Log("Shader failure\n");
        return false;
    }

    // setup texture font atlas
    glGenTextures(1, &m_texId);
    m_texAtlas->SetTexObj(m_texId);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_texId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // load to GPU
    unsigned int cols = 0, rows = 0;
    const void* pPixels = m_texAtlas->GetData(cols, rows);
    const GLint internalformat = (m_texAtlas->GetBpp() == 32) ? GL_RGBA : GL_RGB;
    const GLenum format = (m_texAtlas->GetBpp() == 32) ? GL_RGBA : GL_RGB;
    glTexImage2D(GL_TEXTURE_2D, 0, internalformat, static_cast<GLsizei>(cols), static_cast<GLsizei>(rows), 0, format, GL_UNSIGNED_BYTE, pPixels);
    m_texAtlas->FreeData();
    m_texAtlas->SetReady(true);
    return true;
}

void TextBillboard::SetViewport(const glm::i32vec2& size)
{
    m_viewport.x = static_cast<float>(size.x);
    m_viewport.y = static_cast<float>(size.y);

    m_projection = glm::ortho(-m_viewport.x / 2.0f, m_viewport.x / 2.0f,
                              -m_viewport.y / 2.0f, m_viewport.y / 2.0f,
                              0.1f, 2.0f);
}

void TextBillboard::Render(const glm::mat4 &view)
{
    // the top left corner of the billboard is the top left corner of the screen
    glm::vec4 point(-m_viewport.x / 2.0f, m_viewport.y / 2.0f, -1.0f, 1.0f);
    glm::vec4 topLeft = glm::inverse(view) * point; // to worldspace

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    m_program.Enable();
    m_program.SetBillboardTopLeftCorner(topLeft);
    m_program.SetBillboardScale({ATLAS_GLYPH_WIDTH/m_viewport.x, ATLAS_GLYPH_HEIGHT/m_viewport.y});
    m_program.SetViewProjMatrix(m_projection * view);
    m_program.SetTextureUnit(0);

    glEnableVertexAttribArray(0); // position coords
    glEnableVertexAttribArray(1); // texture coords

    glActiveTexture(GL_TEXTURE0);

    for (const auto& line : m_lines)
    {
        glBindBuffer(GL_ARRAY_BUFFER, line.second.m_vboId);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(0));

        glBindTexture(GL_TEXTURE_2D, m_texAtlas->GetTexObj());
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(sizeof(glm::vec3)));

        glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(line.second.m_vertices.size()));
    }

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    glDisable(GL_BLEND);
}
