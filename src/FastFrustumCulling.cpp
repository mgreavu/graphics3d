#include <graphics3d/FastFrustumCulling.h>

FastFrustumCulling::FastFrustumCulling()
{
}

FastFrustumCulling::~FastFrustumCulling()
{
}

void FastFrustumCulling::NormalizePlaneVector(glm::vec4& aPlane)
{
   float det = glm::sqrt(aPlane.x * aPlane.x + aPlane.y * aPlane.y + aPlane.z * aPlane.z);
   det = 1 / det; // invert to use multiplies below.

   aPlane *= det;
}

void FastFrustumCulling::Load(const glm::mat4& aMVPMatrix)
{
    // left
    m_PlaneVectors[leftPlane].x = aMVPMatrix[0][3] + aMVPMatrix[0][0];
    m_PlaneVectors[leftPlane].y = aMVPMatrix[1][3] + aMVPMatrix[1][0];
    m_PlaneVectors[leftPlane].z = aMVPMatrix[2][3] + aMVPMatrix[2][0];
    m_PlaneVectors[leftPlane].w = aMVPMatrix[3][3] + aMVPMatrix[3][0];
    NormalizePlaneVector(m_PlaneVectors[leftPlane]);

    // right
    m_PlaneVectors[rightPlane].x = aMVPMatrix[0][3] - aMVPMatrix[0][0];
    m_PlaneVectors[rightPlane].y = aMVPMatrix[1][3] - aMVPMatrix[1][0];
    m_PlaneVectors[rightPlane].z = aMVPMatrix[2][3] - aMVPMatrix[2][0];
    m_PlaneVectors[rightPlane].w = aMVPMatrix[3][3] - aMVPMatrix[3][0];
    NormalizePlaneVector(m_PlaneVectors[rightPlane]);

    // bottom
    m_PlaneVectors[bottomPlane].x = aMVPMatrix[0][3] + aMVPMatrix[0][1];
    m_PlaneVectors[bottomPlane].y = aMVPMatrix[1][3] + aMVPMatrix[1][1];
    m_PlaneVectors[bottomPlane].z = aMVPMatrix[2][3] + aMVPMatrix[2][1];
    m_PlaneVectors[bottomPlane].w = aMVPMatrix[3][3] + aMVPMatrix[3][1];
    NormalizePlaneVector(m_PlaneVectors[bottomPlane]);

    // top
    m_PlaneVectors[topPlane].x = aMVPMatrix[0][3] - aMVPMatrix[0][1];
    m_PlaneVectors[topPlane].y = aMVPMatrix[1][3] - aMVPMatrix[1][1];
    m_PlaneVectors[topPlane].z = aMVPMatrix[2][3] - aMVPMatrix[2][1];
    m_PlaneVectors[topPlane].w = aMVPMatrix[3][3] - aMVPMatrix[3][1];
    NormalizePlaneVector(m_PlaneVectors[topPlane]);

    // near
    m_PlaneVectors[nearPlane].x = aMVPMatrix[0][3] + aMVPMatrix[0][2];
    m_PlaneVectors[nearPlane].y = aMVPMatrix[1][3] + aMVPMatrix[1][2];
    m_PlaneVectors[nearPlane].z = aMVPMatrix[2][3] + aMVPMatrix[2][2];
    m_PlaneVectors[nearPlane].w = aMVPMatrix[3][3] + aMVPMatrix[3][2];
    NormalizePlaneVector(m_PlaneVectors[nearPlane]);

    // far
    m_PlaneVectors[farPlane].x = aMVPMatrix[0][3] - aMVPMatrix[0][2];
    m_PlaneVectors[farPlane].y = aMVPMatrix[1][3] - aMVPMatrix[1][2];
    m_PlaneVectors[farPlane].z = aMVPMatrix[2][3] - aMVPMatrix[2][2];
    m_PlaneVectors[farPlane].w = aMVPMatrix[3][3] - aMVPMatrix[3][2];
    NormalizePlaneVector(m_PlaneVectors[farPlane]);
}

int FastFrustumCulling::RectPrismCulled(const BoundingBox& aRectPrism) const
{
    unsigned char flag = 0;
    unsigned char in = 1;
    unsigned char out = 2;
    unsigned char intersect = 3;
    unsigned char inCtr = 0;

    for (int ctr = firstPlane; ctr < planeCnt; ctr++)
    {
        // Clear 'in' to prepare for this test.
        // If previous iteration had an 'out', then this object will be
        // intersecting at best, totally out at worst.
        flag = flag & out;

        // Check each point against this plane.
        flag |= (GetDistance(m_PlaneVectors[ctr], aRectPrism.corner(0)) > 0) ? in : out;
        if (flag == intersect) continue;

        flag |= (GetDistance(m_PlaneVectors[ctr], aRectPrism.corner(1)) > 0) ? in : out;
        if (flag == intersect) continue;

        flag |= (GetDistance(m_PlaneVectors[ctr], aRectPrism.corner(2)) > 0) ? in : out;
        if (flag == intersect) continue;

        flag |= (GetDistance(m_PlaneVectors[ctr], aRectPrism.corner(3)) > 0) ? in : out;
        if (flag == intersect) continue;

        flag |= (GetDistance(m_PlaneVectors[ctr], aRectPrism.corner(4)) > 0) ? in : out;
        if (flag == intersect) continue;

        flag |= (GetDistance(m_PlaneVectors[ctr], aRectPrism.corner(5)) > 0) ? in : out;
        if (flag == intersect) continue;

        flag |= (GetDistance(m_PlaneVectors[ctr], aRectPrism.corner(6)) > 0) ? in : out;
        if (flag == intersect) continue;

        flag |= (GetDistance(m_PlaneVectors[ctr], aRectPrism.corner(7)) > 0) ? in : out;
        if (flag == intersect) continue;

        if (flag == out) return FFC_CULLED; // All points are outside of this plane

        // Intersections jumped to the next iteration, outside already returned,
        // therefore all six points are fully on the inside of this plane
        inCtr++;
    }

    if (inCtr == 6)
    {
        // All points were within the volume
        return FFC_NOT_CULLED;
    }
    else
    {
        // At least one plane was intersected
        return FFC_INTERSECTS;
    }
}

int FastFrustumCulling::SphereCulled(const glm::vec3& aPos, float aRad) const
{
    float dist, minDist;

    minDist = -GetDistance(m_PlaneVectors[firstPlane], aPos);
    if (minDist > aRad)
    {
        // At least "radius" outside of the plane, cull
        return FFC_CULLED;
    }

    for (int ctr = firstPlane+1; ctr < planeCnt; ctr++)
    {
        dist = -GetDistance(m_PlaneVectors[ctr], aPos);
        if (dist > aRad)
        {
            // At least "radius" outside of the plane, cull
            return FFC_CULLED;
        }

        if (dist < minDist)
        {
            // Keep track of point furthest from center of volume (most outside)
            minDist = dist;
        }
    }

    if (minDist < -aRad)
    {
        // At least "radius" inside of each plane, so fully contained
        return FFC_NOT_CULLED;
    }
    else
    {
        // Partially out, but intersects the volume
        return FFC_INTERSECTS;
    }
}

//=============================================================================
int FastFrustumCulling::PointCulled(const glm::vec3& aPos) const
{
    float dist;

    for (int ctr = firstPlane; ctr < planeCnt; ctr++)
    {
        dist = GetDistance(m_PlaneVectors[ctr], aPos);
        if (dist < 0)
        {
            return FFC_CULLED; // The point is outside of this plane, cull
        }
    }
    return FFC_NOT_CULLED;
}

