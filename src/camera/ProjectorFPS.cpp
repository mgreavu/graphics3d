#include <graphics3d/ProjectorFPS.h>
#include <glm/trigonometric.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef DEVELOPER_DEBUG
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#endif

static constexpr glm::vec3 CAM_RIGHT(1.0f, 0.0f, 0.0f);
static constexpr glm::vec3 CAM_UP(0.0f, 1.0f, 0.0f);
static constexpr glm::vec3 CAM_FORWARD(0.0f, 0.0f, -1.0f);

ProjectorFPS::ProjectorFPS(float fovyAngle, float nearPlaneDist, float farPlaneDist)
    : m_fovy(fovyAngle)
    , m_near(nearPlaneDist)
    , m_far(farPlaneDist)
    , m_Pos(0.0f, 0.0f, 1.0f)
    , m_LookAt(0.0f, 0.0f, 0.0f)
    , m_Up(CAM_UP)
    , m_headingAngle(0.0f)
    , m_tiltingAngle(0.0f)
    , m_rotation(1.0f)
{
    m_viewingDirection = glm::normalize(m_LookAt - m_Pos);
}

void ProjectorFPS::SetViewport(const glm::i32vec2& size)
{
    m_viewport = size;

    m_MatProjection = glm::perspective(glm::radians(m_fovy),
                                       static_cast<float>(m_viewport.x) / m_viewport.y,
                                       m_near,
                                       m_far);

#ifdef DEVELOPER_DEBUG
    std::cout << "projection: " << glm::to_string(m_MatProjection) << std::endl;
#endif
}

void ProjectorFPS::Update()
{
    m_LookAt = m_Pos + m_viewingDirection;

    m_MatView = glm::lookAt(m_Pos, m_LookAt, m_Up);
    m_MatMVP = m_MatProjection * m_MatView;

#ifdef DEVELOPER_DEBUG
    std::cout << "modelview: " << glm::to_string(m_MatView) << std::endl;
    std::cout << "mvp: " << glm::to_string(m_MatMVP) << std::endl;
#endif
}

void ProjectorFPS::SetPosition(const glm::vec3& pos)
{
    m_Pos = pos;
}

void ProjectorFPS::SetHeading(float angle)
{
    if (angle < 0) angle += 360;
    if (angle >= 360) angle -= 360;

    m_headingAngle = angle;
    m_rotation = glm::rotate(glm::mat4(1.0f), glm::radians(m_headingAngle), CAM_UP);
    m_rotation = glm::rotate(m_rotation, glm::radians(m_tiltingAngle), CAM_RIGHT);

    // the forward vector is a unit vector and will remain a unit vector after rotation
    m_viewingDirection = glm::mat3(m_rotation) * CAM_FORWARD;
    // the up vector remains a unit vector after rotation
    m_Up = glm::mat3(m_rotation) * CAM_UP;
}

void ProjectorFPS::SetTilting(float angle)
{
    if (angle < 0) angle += 360;
    if (angle >= 360) angle -= 360;

    m_tiltingAngle = angle;
    m_rotation = glm::rotate(glm::mat4(1.0f), glm::radians(m_headingAngle), CAM_UP);
    m_rotation = glm::rotate(m_rotation, glm::radians(m_tiltingAngle), CAM_RIGHT);

    // the forward vector is a unit vector and will remain a unit vector after rotation
    m_viewingDirection = glm::mat3(m_rotation) * CAM_FORWARD;
    // the up vector remains a unit vector after rotation
    m_Up = glm::mat3(m_rotation) * CAM_UP;
}

void ProjectorFPS::MoveForward(float fact)
{
    // scale the normalized lookAt vector then
    // do vector addition, move with a fraction of the lookAt vector
    m_Pos += -fact * m_viewingDirection;
}

void ProjectorFPS::MoveBackward(float fact)
{
    m_Pos += fact * m_viewingDirection;
}

void ProjectorFPS::MoveUp(float fact)
{
    m_Pos += fact * m_Up;
}

void ProjectorFPS::MoveDown(float fact)
{
    m_Pos += -fact * m_Up;
}

void ProjectorFPS::StrafeRight(float fact)
{
    glm::vec3 right = glm::cross(m_viewingDirection, m_Up);
    m_Pos += fact * right;
}

void ProjectorFPS::StrafeLeft(float fact)
{
    glm::vec3 right = glm::cross(m_viewingDirection, m_Up);
    m_Pos += -fact * right;
}

glm::vec3 ProjectorFPS::Raycast(const glm::i32vec2& pos) const
{
    // to a NDC point, reverse viewport transform
    float x = (2.0f * pos.x) / m_viewport.x - 1.0f;
    float y = 1.0f - (2.0f * pos.y) / m_viewport.y;
    float z = 1.0f;
    glm::vec3 ndcPoint(x, y, z);

    // to a clip space point
    glm::vec4 clipPoint(ndcPoint.x, ndcPoint.y, -1.0f, 1.0f);

    // to a camera space point, then to a forward ray (vector)
    glm::vec4 cameraPoint = glm::inverse(m_MatProjection) * clipPoint;
    glm::vec4 cameraRay = glm::vec4(cameraPoint.x, cameraPoint.y, -1.0f, 0.0f);

    // to 4D World Coordinates
    glm::vec3 worldRay = glm::inverse(m_MatView) * cameraRay;
    worldRay = glm::normalize(worldRay);

    return worldRay;
}
