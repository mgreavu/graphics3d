#include <graphics3d/Texture2D.h>
#include <FreeImage.h>

#include <algorithm>

Texture2D::Texture2D()
    : m_ready(false)
    , m_texObj(-1)
    , m_width(0)
    , m_height(0)
    , m_pitch(0)
    , m_bpp(0)
    , m_hasAlpha(false)
    , m_imageData(nullptr)
{
}

Texture2D::~Texture2D()
{
    FreeData();
}

bool Texture2D::Load(const std::string& fileName)
{
    _ASSERT(m_imageData == nullptr);

    FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(fileName.c_str(), 0);
    if (fif == FIF_UNKNOWN)
    {
        fif = FreeImage_GetFIFFromFilename(fileName.c_str());
    }

    FIBITMAP* pImage = FreeImage_Load(fif, fileName.c_str());
    if (!pImage)
    {
        // SDL_Log("Texture2D::Load() --> FAILED: \"%s\"\n", fileName.c_str());
        return false;
    }

    m_bpp = FreeImage_GetBPP(pImage);
    if (m_bpp != 24 && m_bpp != 32)
    {
        FIBITMAP* pTempImage = pImage;
        pImage = FreeImage_ConvertTo32Bits(pTempImage); // makes a clone
        FreeImage_Unload(pTempImage);
        if (!pImage)
        {
            // SDL_Log("Texture2D::Load() --> FreeImage_ConvertTo32Bits() failed: \"%s\"\n", fileName.c_str());
            return false;
        }
        m_bpp = 32;
    }

    m_width = FreeImage_GetWidth(pImage);
    m_height = FreeImage_GetHeight(pImage);
    // Returns the width of the bitmap in bytes, rounded to the next 32-bit boundary.
    // In FreeImage each scanline starts at a 32-bit boundary for performance reasons.
    m_pitch = FreeImage_GetPitch(pImage);

    m_imageData = std::make_unique<uint8_t[]>(m_height * m_pitch);
    FreeImage_ConvertToRawBits(m_imageData.get(), pImage, m_pitch, m_bpp, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);
    FreeImage_Unload(pImage);

    const uint8_t pixel_size = m_bpp / 8;
    for (unsigned int pix = 0; pix < m_height * m_width * pixel_size; pix += pixel_size)  // convert from BGRA to RGBA
    {
        std::swap(m_imageData[pix], m_imageData[pix + 2]);
    }

    m_hasAlpha = (m_bpp == 32) ? HasAlpha(m_imageData.get(), m_width, m_height) : false;
    // SDL_Log("Texture2D::Load() --> \"%s\", bpp = %d, hasAlpha = %s\n", fileName.c_str(), m_bpp, m_hasAlpha ? "true" : "false");
    return true;
}

void Texture2D::FreeData()
{
    if (m_imageData)
    {
        m_imageData.reset();
        // SDL_Log("Texture2D::FreeData() --> Freed %u bytes\n", m_height * m_pitch);
        m_width = 0;
        m_height = 0;
        m_pitch = 0;
        m_bpp = 0;
    }
}

bool Texture2D::HasAlpha(const unsigned char* pData, unsigned int width, unsigned int height) const
{
    unsigned int stepX = 8, stepY = 8;
    if (width < 8) { stepX = width; }
    if (height < 8) { stepY = height; }

    const unsigned int strideX = width / stepX;
    const unsigned int strideY = height / stepY;

    unsigned int alphaCount = 0;
    for (size_t w = 0; w < width; w += strideX)
    {
        for (size_t h = 0; h < height; h += strideY)
        {
            if (pData[w * h + 3] != 0xFF)
            {
                ++alphaCount;
            }
        }
    }
    return (alphaCount > 0.1f * stepX * stepY);
}

const void* Texture2D::GetData(unsigned int& width, unsigned int& height) const
{
    width = m_width;
    height = m_height;
    return m_imageData.get();
}
