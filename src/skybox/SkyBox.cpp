#include <graphics3d/SkyBox.h>
#include <FreeImage.h>
// #include <SDL2/SDL_log.h>
#include <vector>
#include <memory>

static constexpr GLfloat skyboxVertices[] = {
    // back
    -1.0f,  1.0f, -1.0f,
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    // left
    -1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f, -1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,
    // right
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    // front
    -1.0f, -1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f, -1.0f,  1.0f,
    -1.0f, -1.0f,  1.0f,
    // top
    -1.0f,  1.0f, -1.0f,
    1.0f,  1.0f, -1.0f,
    1.0f,  1.0f,  1.0f,
    1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f,  1.0f,
    -1.0f,  1.0f, -1.0f,
    // bottom
    -1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    -1.0f, -1.0f,  1.0f,
    1.0f, -1.0f,  1.0f
};


SkyBox::SkyBox(const std::string& ext)
    : m_ext(ext)
    , m_vboId(0)
    , m_texId(0)
{
}

SkyBox::~SkyBox()
{
    // glDeleteTextures silently ignores 0's and names that do not correspond to existing textures
    glDeleteTextures(1, &m_texId);
    // glDeleteBuffers silently ignores 0's and names that do not correspond to existing buffer objects
    glDeleteBuffers(1, &m_vboId);
}

bool SkyBox::Load(const std::string &path)
{
    if (path.empty())
    {
        // SDL_Log("Loading of skybox failed, textures path\n");
        return false;
    }
    if (!m_program.Init())
    {
        // SDL_Log("Loading of skybox failed, skybox shader\n");
        return false;
    }

    glGenBuffers(1, &m_vboId);
    glBindBuffer(GL_ARRAY_BUFFER, m_vboId);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);

    // https://www.khronos.org/opengl/wiki/Cubemap_Texture
    //  glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &m_texId);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_texId);

    // the width and height of a cubemap must be the same (ie: cubemaps are squares), but these sizes need not be powers of two
    const std::vector<std::string> faces { path + std::string("right.") + m_ext,
                                           path + std::string("left." + m_ext),
                                           path + std::string("top." + m_ext),
                                           path + std::string("bottom." + m_ext),
                                           path + std::string("back." + m_ext),
                                           path + std::string("front." + m_ext) };

    for (unsigned int i = 0; i < faces.size(); ++i)
    {
        FREE_IMAGE_FORMAT fif = FreeImage_GetFileType(faces[i].c_str(), 0);
        if (fif == FIF_UNKNOWN)
        {
            fif = FreeImage_GetFIFFromFilename(faces[i].c_str());
        }

        FIBITMAP* pImage = FreeImage_Load(fif, faces[i].c_str());
        if (pImage)
        {
            if (FreeImage_GetBPP(pImage) != 24)
            {
                FIBITMAP* pTempImage = pImage;
                pImage = FreeImage_ConvertTo24Bits(pTempImage); // makes a clone
                FreeImage_Unload(pTempImage);
            }

            assert(FreeImage_GetBPP(pImage) == 24);

            unsigned int width = FreeImage_GetWidth(pImage);
            unsigned int height = FreeImage_GetHeight(pImage);
            unsigned int pitch = FreeImage_GetPitch(pImage);

            auto imageData = std::make_unique<uint8_t[]>(height * pitch);
            FreeImage_ConvertToRawBits(imageData.get(), pImage, pitch, 24, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, TRUE); // convert and flip
            FreeImage_Unload(pImage);

            const uint8_t pixel_size = 3;
            for (unsigned int pix = 0; pix < height * width * pixel_size; pix += pixel_size)  // convert from BGR to RGB
            {
                std::swap(imageData[pix], imageData[pix + 2]);
            }

            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, static_cast<GLsizei>(width), static_cast<GLsizei>(height), 0, GL_RGB, GL_UNSIGNED_BYTE, imageData.get());
        }
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    m_program.Enable();
    m_program.SetTextureUnit(0);
    return true;
}

void SkyBox::Render(const glm::mat4& mvp)
{
/*
    GL_DEPTH_FUNC
    params returns one value, the symbolic constant that indicates the depth comparison function.
    The initial value is GL_LESS. See glDepthFunc.
*/
    GLint oldDepthFuncMode;
    glGetIntegerv(GL_DEPTH_FUNC, &oldDepthFuncMode);

    // Passes if the incoming depth value is less than or equal to the stored depth value
    glDepthFunc(GL_LEQUAL);

    m_program.Enable();
    m_program.SetMVP(mvp);

    glEnableVertexAttribArray(0); // vertices
    glBindBuffer(GL_ARRAY_BUFFER, m_vboId);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), reinterpret_cast<GLvoid*>(0));

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, m_texId);

    glDrawArrays(GL_TRIANGLES, 0, 36);
    glDisableVertexAttribArray(0); // vertices

/*
    glDepthFunc specifies the function used to compare each incoming pixel depth value with the depth value present in the depth buffer.
    The comparison is performed only if depth testing is enabled. (See glEnable and glDisable of GL_DEPTH_TEST)
*/
    glDepthFunc(static_cast<GLenum>(oldDepthFuncMode));
}
