#include <graphics3d/SkyboxProgram.h>
#include <glm/gtc/type_ptr.hpp>

bool SkyboxProgram::Init()
{
    bool ok = GlslProgram::Init();
    ok = ok && AddShader(GL_VERTEX_SHADER, "shaders/skybox.vs");
    ok = ok && AddShader(GL_FRAGMENT_SHADER, "shaders/skybox.fs");
    ok = ok && Finalize();

    if (ok)
    {
        m_MVPLocation = GetUniformLocation("gMVP");
        m_samplerLocation = GetUniformLocation("cubeMap");
        if (m_MVPLocation == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_samplerLocation == static_cast<GLint>(INVALID_UNIFORM_LOCATION))
        {
            ok = false;
        }
    }
    return ok;
}

void SkyboxProgram::SetMVP(const glm::mat4& MVP)
{
    glUniformMatrix4fv(m_MVPLocation, 1, GL_FALSE, glm::value_ptr(MVP));
}

void SkyboxProgram::SetTextureUnit(GLint textureUnit)
{
    glUniform1i(m_samplerLocation, textureUnit);
}
