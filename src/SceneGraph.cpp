#include <graphics3d/SceneGraph.h>
#include <graphics3d/Model3d.h>
// #include <SDL2/SDL_log.h>
#include <glm/gtx/intersect.hpp>

SceneGraph::SceneGraph()
    : m_root(std::make_unique<SceneNode>())
{
}

SceneNode* SceneGraph::GetRoot() const
{
    return m_root.get();
}

SceneNode* SceneGraph::Find(const std::string& id) const
{
    return Find(m_root.get(), id);
}

void SceneGraph::Select(const glm::vec3& rayOrigin, const glm::vec3& rayDir, float& minDist, SceneNode** pNearestNode) const
{
    Select(m_root.get(), rayOrigin, rayDir, minDist, pNearestNode);
}

void SceneGraph::Update()
{
    m_root->Update();
}

SceneNode* SceneGraph::Find(SceneNode* fromNode, const std::string& id) const
{
    if (!fromNode)
    {
        return nullptr;
    }

    const Model3d* pModel = fromNode->GetModel();
    if (pModel && pModel->GetName() == id)
    {
        return fromNode;
    }

    SceneNode* pFound = nullptr;
    for (SceneNode::const_iterator it = fromNode->cbegin(); it != fromNode->cend(); ++it)
    {
        if ((pFound = Find((*it).get(), id)) != nullptr)
        {
            break;
        }
    }
    return pFound;
}

void SceneGraph::Select(SceneNode* fromNode, const glm::vec3 &rayOrigin, const glm::vec3 &rayDir, float &minDist, SceneNode **pNearestNode) const
{
    static constexpr unsigned int BBOX_FACE_INDICES[36] = {
        0, 4, 6, 6, 2, 0,
        1, 5, 7, 7, 3, 1,
        2, 6, 7, 7, 3, 2,
        0, 4, 5, 5, 1, 0,
        0, 2, 1, 2, 3, 1,
        4, 5, 6, 5, 6, 7
    }; // 6 rectangular bbox sides

    const Model3d* pModel = fromNode->GetModel();
    if (pModel)
    {
        const BoundingBox& bbox = pModel->GetBBox();
        const glm::mat4& worldTransf = fromNode->GetWorldTransform();

        bool hit = false;
        glm::vec2 baryPos;
        float distance = std::numeric_limits<float>::max();
        float minHitDist = std::numeric_limits<float>::max();

        for (unsigned i = 0; i < 36; i += 3)
        {
            if (!glm::intersectRayTriangle(
                                rayOrigin,
                                rayDir,
                                glm::vec3(worldTransf * glm::vec4(bbox.corner(BBOX_FACE_INDICES[i]), 1.0f)),
                                glm::vec3(worldTransf * glm::vec4(bbox.corner(BBOX_FACE_INDICES[i + 1]), 1.0f)),
                                glm::vec3(worldTransf * glm::vec4(bbox.corner(BBOX_FACE_INDICES[i + 2]), 1.0f)),
                                baryPos,
                                distance))
            {
                continue;
            }

            hit = true;
            minHitDist = std::min(minHitDist, distance);
        }
        if (hit)
        {
            // SDL_Log("Hit test good model: %s, dist: %.4f\n", pModel->GetName().c_str(), static_cast<double>(minHitDist));
            if (minHitDist > 0 && minHitDist < minDist)
            {
                minDist = minHitDist;
                *pNearestNode = fromNode;
            }
        }
    }

    for (SceneNode::const_iterator cit = fromNode->cbegin(); cit != fromNode->cend(); ++cit)
    {
        Select((*cit).get(), rayOrigin, rayDir, minDist, pNearestNode);
    }
}
