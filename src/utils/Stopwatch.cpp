#include <graphics3d/Stopwatch.h>

graphics_engine::Stopwatch::Stopwatch()
    : m_start(std::chrono::steady_clock::now())
    , m_stop(m_start)
{
}

void graphics_engine::Stopwatch::Start()
{
    m_start = std::chrono::steady_clock::now();
    m_stop = m_start;
}

uint64_t graphics_engine::Stopwatch::Restart()
{
    m_stop = std::chrono::steady_clock::now();
    uint64_t duration = std::chrono::duration_cast<std::chrono::duration<uint64_t, std::milli>>(m_stop - m_start).count();
    m_start = m_stop;
    return duration;
}

uint64_t graphics_engine::Stopwatch::Stop()
{
    m_stop = std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::duration<uint64_t, std::milli>>(m_stop - m_start).count();
}

uint64_t graphics_engine::Stopwatch::Elapsed() const
{
    const std::chrono::time_point<std::chrono::steady_clock> now = std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::duration<uint64_t, std::milli>>(now - m_start).count();
}
