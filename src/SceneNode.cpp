#include <graphics3d/SceneNode.h>
#include <graphics3d/Model3d.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

static const glm::vec3 XAXIS(1.0f, 0.0f, 0.0f);
static const glm::vec3 YAXIS(0.0f, 1.0f, 0.0f);
static const glm::vec3 ZAXIS(0.0f, 0.0f, 1.0f);

SceneNode::SceneNode()
    : m_parent(nullptr)
    , m_pModel3d(nullptr)
    , m_transform(1.0f) // identity
    , m_worldTransform(1.0f) // identity
    , m_origin(0.0f)
    , m_translation(0.0f)
    , m_rotation(1.0f) // identity
{
}

SceneNode::SceneNode(std::shared_ptr<Model3d> pModel)
    : m_parent(nullptr)
    , m_pModel3d(pModel)
    , m_transform(1.0f) // identity
    , m_worldTransform(1.0f) // identity
    , m_origin(0.0f)
    , m_translation(0.0f)
    , m_rotation(1.0f) // identity
{
}

void SceneNode::Reserve(size_t size)
{
    m_children.reserve(size);
}

void SceneNode::AddChild(std::unique_ptr<SceneNode> node)
{
    node->m_parent = this;
    m_children.push_back(std::move(node));
}

void SceneNode::Update()
{
    m_transform = glm::mat4(1.0f); // identity
    m_transform = glm::translate(m_transform, m_translation);
    m_transform *= m_rotation;
    m_transform = glm::translate(m_transform, glm::vec3(-m_origin.x, -m_origin.y, -m_origin.z));

    if (m_parent)
    {
         m_worldTransform = m_parent->m_worldTransform * m_transform;
    }
    else
    {
         m_worldTransform = m_transform;
    }

    for (auto& node : m_children)
    {
        node->Update();
    }
}

void SceneNode::SetOrigin(const glm::vec3& origin)
{
    m_origin = origin;
}

void SceneNode::SetTranslation(const glm::vec3& translation)
{
    m_translation = translation;
}

void SceneNode::SetRotation(float rotX, float rotY, float rotZ)
{
    m_rotation = glm::rotate(glm::mat4(1.0f), glm::radians(rotX), XAXIS);
    m_rotation = glm::rotate(m_rotation, glm::radians(rotY), YAXIS);
    m_rotation = glm::rotate(m_rotation, glm::radians(rotZ), ZAXIS);
}

void SceneNode::SetRotation(float angle, const glm::vec3& axis)
{
    glm::quat angleAxis = glm::angleAxis(angle, glm::normalize(axis));
    m_rotation = glm::toMat4(angleAxis);
}
