#version 330

layout (location = 0) in vec3 position;

out vec3 TexCoords;

uniform mat4 gMVP;

void main()
{
/*
	What happens here is that after the vertex shader is complete the rasterizer takes gl_Position vector and performs
	perspective divide (division by W) in order to complete the projection. When we set Z to W we guarantee that the final Z value
	of the position will be 1.0. This Z value is always mapped to the far Z.
	This means that the skybox will always fail the depth test against the other models in the scene.
	That way the skybox will only take up the background left between the models and everything else will be infront of it,
	which is exactly what we expect from it. 
*/
	vec4 MVP_Pos = gMVP * vec4(position, 1.0);
	gl_Position = MVP_Pos.xyww;

//	the texture coordinate used to access a cubemap is a 3D direction vector which represents a direction from the center of the cube to the value to be accessed
	TexCoords = position;
}
