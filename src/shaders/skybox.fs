#version 330

in vec3 TexCoords;
out vec4 color;

uniform samplerCube cubeMap;

void main()
{    
    color = texture(cubeMap, TexCoords);
}
