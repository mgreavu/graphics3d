#version 330 core
out vec4 FragColor;

struct Material
{
	bool useDiffuseTex;
	sampler2D diffuse;
	bool useSpecularTex;
	sampler2D specular;
	float shininess;
};

struct Light
{
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

uniform vec3 viewPos;
uniform Material material;
uniform Light light;

void main()
{
	vec4 result;

	if (material.useDiffuseTex && !material.useSpecularTex)
	{
		vec3 ambient = light.ambient * texture(material.diffuse, TexCoords).rgb;

		// diffuse
		vec3 norm = normalize(Normal);
		vec3 lightDir = normalize(light.position - FragPos);
		float diff = max(dot(norm, lightDir), 0.0);
		vec4 diffuse = vec4(light.diffuse, 1.0) * diff * texture(material.diffuse, TexCoords).rgba;
		result = vec4(ambient, 0.0) + diffuse;
	}
	else if (material.useDiffuseTex && material.useSpecularTex) // ignoring transparency, alpha value
	{
		vec3 ambient = light.ambient * texture(material.diffuse, TexCoords).rgb;

		// diffuse
		vec3 norm = normalize(Normal);
		vec3 lightDir = normalize(light.position - FragPos);
		float diff = max(dot(norm, lightDir), 0.0);
		vec3 diffuse = light.diffuse * diff * texture(material.diffuse, TexCoords).rgb;

		// specular
		vec3 viewDir = normalize(viewPos - FragPos);
		vec3 reflectDir = reflect(-lightDir, norm);
		float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
		vec3 specular = light.specular * spec * texture(material.specular, TexCoords).rgb;
		result = vec4(ambient + diffuse + specular, 1.0f);
	}
	else
	{
		result = vec4(light.ambient + light.diffuse, 1.0);
	}

    FragColor = result;
}
