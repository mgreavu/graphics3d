#pragma once

#include <graphics3d/Material.h>
#include <graphics3d/Vertex.h>
#include <graphics3d/BoundingBox.h>

#include <GL/glew.h>

#include <vector>
#include <memory>

struct Mesh
{
    typedef std::vector<Mesh>::const_iterator const_iterator;
    typedef std::vector<Mesh>::iterator iterator;

    enum { INVALID_MATERIAL = 0xFFFFFFFF };

    Mesh()
        : m_VB(0)
        , m_IB(0)
        , m_MaterialIndex(INVALID_MATERIAL)
        , m_pMaterial(nullptr)
    {
    }

    Mesh(const Mesh& other) = delete;
    Mesh& operator=(const Mesh& other) = delete;

    Mesh(Mesh&& other) = default;
    Mesh& operator=(Mesh&& other) = default;

    GLuint m_VB;
    GLuint m_IB;

    std::vector<Vertex>       m_vertices;
    std::vector<unsigned int> m_indices;

    unsigned int         m_MaterialIndex;
    std::shared_ptr<Material> m_pMaterial;

    BoundingBox m_bbox; // the bounding box of this mesh
};
