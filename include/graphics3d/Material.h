#pragma once
#include <graphics3d/Texture2D.h>

#include <glm/vec4.hpp>

#include <string>
#include <memory>

struct Material
{
    Material()
        : m_Ka(0.0f)
        , m_Kd(0.0f)
        , m_Ks(0.0f)
        , m_shininess(0.0f)
        , m_opacity(1.0f)
        , m_pTexDiffuse(nullptr)
        , m_pTexSpecular(nullptr)
        , m_pTexNormals(nullptr)
    {
    }

    std::string m_name;

    // https://www.loc.gov/preservation/digital/formats/fdd/fdd000508.shtml
    glm::vec4 m_Ka;     // ambient color
    glm::vec4 m_Kd;     // diffuse color
    glm::vec4 m_Ks;     // specular color
    float m_shininess;  // Ns: defines the focus of specular highlights in the material
                        // Ns values normally range from 0 to 1000, with a high value resulting in a tight, concentrated highlight
    float m_opacity;    // d: a factor for dissolve, how much this material dissolves into the background
                        // A factor of 1.0 is fully opaque. A factor of 0.0 is completely transparent.

    std::shared_ptr<Texture2D> m_pTexDiffuse;
    std::shared_ptr<Texture2D> m_pTexSpecular;
    std::shared_ptr<Texture2D> m_pTexNormals;
};
