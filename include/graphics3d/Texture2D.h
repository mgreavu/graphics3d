#pragma once

#include <GL/glew.h>

#include <string>
#include <memory>

class Texture2D
{
public:
    Texture2D();
    ~Texture2D();

    bool Load(const std::string& fileName);
    void FreeData();

    bool IsReady() const { return m_ready; }
    void SetReady(bool ready) { m_ready = ready; }

    void SetTexObj(GLuint texObj) { m_texObj = texObj; }
    GLuint GetTexObj() const { return m_texObj; }

    const void* GetData(unsigned int& width, unsigned int& height) const;
    unsigned int GetBpp() const { return m_bpp; }
    bool HasAlpha() const { return m_hasAlpha; }

private:
    bool HasAlpha(const unsigned char* pData, unsigned int width, unsigned int height) const;

    bool m_ready;
    GLuint m_texObj;

    unsigned int m_width;
    unsigned int m_height;
    unsigned int m_pitch;
    unsigned int m_bpp;
    bool m_hasAlpha;
    std::unique_ptr<uint8_t[]> m_imageData;
};
