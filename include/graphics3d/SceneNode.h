#pragma once
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include <memory>

class Model3d;

class SceneNode
{
public:
    typedef std::vector<std::unique_ptr<SceneNode>>::const_iterator const_iterator;
    typedef std::vector<std::unique_ptr<SceneNode>>::iterator iterator;

    SceneNode();
    SceneNode(std::shared_ptr<Model3d> pModel);

    SceneNode(const SceneNode& other) = delete;
    SceneNode& operator=(const SceneNode& other) = delete;

    const glm::mat4& GetTransform() const { return m_transform; }
    const glm::mat4& GetWorldTransform() const { return m_worldTransform; }

    void SetOrigin(const glm::vec3& origin);
    void SetTranslation(const glm::vec3& translation);
    void SetRotation(float rotX, float rotY, float rotZ);
    void SetRotation(float angle, const glm::vec3& axis);

    Model3d* GetModel() { return m_pModel3d.get(); }
    const Model3d* GetModel() const { return m_pModel3d.get(); }
    void SetModel(std::shared_ptr<Model3d> pModel) { m_pModel3d = pModel; }

    void Reserve(size_t size);
    void AddChild(std::unique_ptr<SceneNode> node);

    const_iterator cbegin() const
    {
        return m_children.cbegin();
    }

    const_iterator cend() const
    {
        return m_children.cend();
    }

    iterator begin()
    {
        return m_children.begin();
    }

    iterator end()
    {
        return m_children.end();
    }

    void Update();

protected:
    SceneNode* m_parent;
    std::vector<std::unique_ptr<SceneNode>> m_children;

    std::shared_ptr<Model3d> m_pModel3d;

    glm::mat4 m_transform;
    glm::mat4 m_worldTransform;

    glm::vec3 m_origin;
    glm::vec3 m_translation;
    glm::mat4 m_rotation;
 };
