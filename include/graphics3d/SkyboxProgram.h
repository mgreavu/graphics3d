#pragma once
#include <graphics3d/GlslProgram.h>
#include "glm/mat4x4.hpp"

class SkyboxProgram : public GlslProgram
{
public:
    SkyboxProgram() = default;
    ~SkyboxProgram() override = default;

    bool Init() override;

    void SetMVP(const glm::mat4& MVP);
    void SetTextureUnit(GLint textureUnit);

protected:
    GLint m_MVPLocation;
    GLint m_samplerLocation;
};

