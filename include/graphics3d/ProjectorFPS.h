#pragma once
#include <graphics3d/Projector.h>

class ProjectorFPS : public Projector
{
public:
    ProjectorFPS(float fovyAngle, float nearPlaneDist, float farPlaneDist);
    ~ProjectorFPS() override = default;

    void SetViewport(const glm::i32vec2& size) override;
    const glm::i32vec2& GetViewport() const override { return m_viewport; }

    const glm::vec3& GetCameraPosition() const override { return m_Pos; }
    const glm::vec3& GetCameraTarget() const override { return m_LookAt; }
    const glm::vec3& GetCameraUp() const override { return m_Up; }

    void SetPosition(const glm::vec3& pos) override;
    void SetHeading(float angle) override;
    float GetHeading() const override { return m_headingAngle; }

    void SetTilting(float angle) override;
    float GetTilting() const override { return m_tiltingAngle; }

    void MoveForward(float fact) override;
    void MoveBackward(float fact) override;

    void MoveUp(float fact) override;
    void MoveDown(float fact) override;

    void StrafeRight(float fact) override;
    void StrafeLeft(float fact) override;

    const glm::mat4& GetViewMatrix() const override { return m_MatView; }
    const glm::mat4& GetProjectionMatrix() const override { return m_MatProjection; }
    const glm::mat4& GetCombinedMVPMatrix() const override { return m_MatMVP; }

    glm::vec3 Raycast(const glm::i32vec2& pos) const override;

    void Update() override;

protected:
    float m_fovy;
    float m_near;
    float m_far;

    glm::i32vec2 m_viewport;

    glm::vec3 m_Pos;
    glm::vec3 m_LookAt;
    glm::vec3 m_Up;

    glm::vec3 m_viewingDirection;

    float m_headingAngle;
    float m_tiltingAngle;
    glm::mat4 m_rotation;

/*
    In 3D graphics, you need to worry about several vector spaces:
    Model space      - these are usually the coordinates you specify to OpenGL
    World space      - coordinates are specified with respect to some central point in the world.
    View space       - coordinates are specified with respect to the camera
    Projection space - everything on the screen fits in the interval [-1, +1] in each dimension.
*/

    glm::mat4 m_MatView;
    glm::mat4 m_MatProjection;  // view to projection space
    glm::mat4 m_MatMVP;
};
