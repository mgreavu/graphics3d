#pragma once
#include <graphics3d/Mesh.h>
#include <graphics3d/BoundingBox.h>

#include <glm/mat4x4.hpp>

#include <vector>
#include <string>
#include <memory>
#include <map>

struct aiScene;
struct aiNode;
struct aiMesh;

struct Material;
class Texture2D;
class Model3d;

class ModelLoader
{
public:
    ModelLoader();
    ~ModelLoader();

    std::shared_ptr<Model3d> Load(const std::string& name, const std::string& fileName, float scale);

private:
    void InitNode(const aiNode* pNode, const aiScene* pScene, const glm::mat4& parentTransform);
    void InitMesh(const aiMesh* paiMesh, const glm::mat4& transform);
    void InitMaterials(const aiScene* pScene, const std::string& fileName);
    BoundingBox MakeBBox() const;
    void Clear();

    unsigned int m_meshIndex;
    glm::mat4 m_scaleTransform;

    std::vector<Mesh> m_meshes; // https://www.cppstories.com/2014/05/vector-of-objects-vs-vector-of-pointers/

    const unsigned int m_postProcessFlags;

    std::map<std::string, std::shared_ptr<Texture2D>> m_mapTex; // texture filepath to Texture pointer
    std::map<unsigned int, std::shared_ptr<Material>> m_mapMat; // material index to Material pointer
};
