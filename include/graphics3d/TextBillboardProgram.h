#pragma once
#include <graphics3d/GlslProgram.h>

#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"


class TextBillboardProgram : public GlslProgram
{
public:
    TextBillboardProgram() = default;
    ~TextBillboardProgram() override = default;

    bool Init() override;

    void SetViewProjMatrix(const glm::mat4& viewProj); // combined camera projection transform
    void SetBillboardTopLeftCorner(const glm::vec3& vec);
    void SetBillboardScale(const glm::vec2& vec);
    void SetTextureUnit(GLint textureUnit);

private:
    GLint m_locViewProjMatrix;
    GLint m_locTopLeft;
    GLint m_locScale;
    GLint m_locSampler;
};
