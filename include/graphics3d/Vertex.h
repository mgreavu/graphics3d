#pragma once
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

struct Vertex
{
    glm::vec3 m_pos;
    glm::vec2 m_tex;
    glm::vec3 m_norm;

    Vertex(const glm::vec3& pos, const glm::vec2& tex)
        : m_pos(pos)
        , m_tex(tex)
    {
    }

    Vertex(const glm::vec3& pos, const glm::vec2& tex, const glm::vec3& norm)
        : m_pos(pos)
        , m_tex(tex)
        , m_norm(norm)
    {
    }

    Vertex(const glm::vec3& pos)
        : m_pos(pos)
    {
    }

    Vertex(const Vertex& other) = delete;
    Vertex& operator=(const Vertex& other) = delete;

    Vertex(Vertex&& other) = default;
    Vertex& operator=(Vertex&& other) = default;
};
