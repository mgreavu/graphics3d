#pragma once
#include <graphics3d/SkyboxProgram.h>
#include <string>

class SkyBox
{
public:
    SkyBox(const std::string& ext);
    ~SkyBox();

    bool Load(const std::string& path);
    void Render(const glm::mat4& mvp);

protected:
    const std::string m_ext;
    GLuint m_vboId;
    GLuint m_texId;
    SkyboxProgram m_program;
};

