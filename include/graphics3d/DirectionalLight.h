#pragma once

#include <glm/vec3.hpp>

/*
    We will specify the color of light sources as a trio of floating point values in the range [0-1].
    By multiplying the color of light by the color of the object we get the reflected color.
    However, we also want to take the ambient intensity of light into account.
    Therefore, the ambient intensity will be specified as a single floating point value in the range [0-1]
    which will also be multiplied by all channels of the reflected color that we've just calculated.
*/
struct DirectionalLight
{
    glm::vec3 m_color;
    float     m_ambientIntensity;
    glm::vec3 m_direction;
    float     m_diffuseIntensity;
};
