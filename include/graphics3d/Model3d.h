#pragma once
#include <graphics3d/Mesh.h>
#include <graphics3d/BoundingBox.h>
#include <string>
#include <vector>

#ifdef MODEL_STATS
// #include <SDL2/SDL_log.h>
#endif

class Model3d
{
public:
    Model3d(const std::string& name, const BoundingBox& bbox, std::vector<Mesh>&& geometries)
        : m_name(name)
        , m_bbox(bbox)
        , m_geometries(std::move(geometries)) // it's especially important to bear in mind that a parameter is always an lvalue,
                                              // even if its type is an rvalue reference, https://en.cppreference.com/w/cpp/language/value_category
    {
#ifdef MODEL_STATS
/*
        size_t vertices = 0;
        Mesh::const_iterator cmit = m_geometries.begin();
        for (; cmit != m_geometries.end(); ++cmit)
        {
            vertices += (*cmit).m_vertices.size();
        }
        const glm::vec3& center(m_bbox.center());
        SDL_Log("name: %s, meshes = %lu, vertices = %lu, center(x,y,z) = (%.4f, %.4f, %.4f), bbox(min, max) = {(%.4f, %.4f, %.4f), (%.4f, %.4f, %.4f)}\n",
                m_name.c_str(), m_geometries.size(), vertices,
                static_cast<double>(center.x), static_cast<double>(center.y), static_cast<double>(center.z),
                static_cast<double>(m_bbox._min.x), static_cast<double>(m_bbox._min.y), static_cast<double>(m_bbox._min.z),
                static_cast<double>(m_bbox._max.x), static_cast<double>(m_bbox._max.y), static_cast<double>(m_bbox._max.z));
*/
#endif
    }

    ~Model3d()
    {}

    Mesh::const_iterator cbegin() const
    {
        return m_geometries.cbegin();
    }

    Mesh::const_iterator cend() const
    {
        return m_geometries.cend();
    }

    Mesh::iterator begin()
    {
        return m_geometries.begin();
    }

    Mesh::iterator end()
    {
        return m_geometries.end();
    }

    const std::string& GetName() const { return m_name; }
    const BoundingBox& GetBBox() const { return m_bbox; }

private:
    std::string  m_name;
    BoundingBox  m_bbox;
    std::vector<Mesh> m_geometries;
};
