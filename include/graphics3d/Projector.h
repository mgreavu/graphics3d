#pragma once
#include <glm/fwd.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class Projector
{
public:
    virtual ~Projector() = default;

    virtual void SetViewport(const glm::i32vec2& size) = 0;
    virtual const glm::i32vec2& GetViewport() const = 0;

    virtual const glm::vec3& GetCameraPosition() const = 0;
    virtual const glm::vec3& GetCameraTarget() const = 0;
    virtual const glm::vec3& GetCameraUp() const = 0;

    virtual void SetPosition(const glm::vec3& pos) = 0;
    virtual void SetHeading(float angle ) = 0;
    virtual float GetHeading() const = 0;

    virtual void SetTilting(float angle) = 0;
    virtual float GetTilting() const = 0;

    virtual void MoveForward(float fact) = 0;
    virtual void MoveBackward(float fact) = 0;

    virtual void MoveUp(float fact) = 0;
    virtual void MoveDown(float fact) = 0;

    virtual void StrafeRight(float fact) = 0;
    virtual void StrafeLeft(float fact) = 0;

    virtual const glm::mat4& GetViewMatrix() const = 0;
    virtual const glm::mat4& GetProjectionMatrix() const = 0;
    virtual const glm::mat4& GetCombinedMVPMatrix() const = 0;

    virtual glm::vec3 Raycast(const glm::i32vec2& pos) const = 0;

    virtual void Update() = 0;
};
