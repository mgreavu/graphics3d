#pragma once

enum RenderingFlags
{
    FLAG_DRAW_WIREFRAME = 0x01,
    FLAG_DRAW_BBOX      = 0x02,
    FLAG_ENABLE_LIGHTS  = 0x04,
    FLAG_ENBLE_TEXTURES = 0x08
};

enum class CullMode : unsigned char
{
    CULL_NONE,
    CULL_BACK,
    CULL_FRONT
};
