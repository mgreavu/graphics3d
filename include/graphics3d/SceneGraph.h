#pragma once
#include <graphics3d/SceneNode.h>
#include <string>
#include <memory>

class SceneGraph
{
public:
    SceneGraph();

    SceneNode* GetRoot() const;
    SceneNode* Find(const std::string& id) const; // in the entire tree
    void Select(const glm::vec3& rayOrigin, const glm::vec3& rayDir, float& minDist, SceneNode** pNearestNode) const;
    void Update();

private:
    SceneNode* Find(SceneNode* fromNode, const std::string& id) const;
    void Select(SceneNode* fromNode, const glm::vec3& rayOrigin, const glm::vec3& rayDir, float& minDist, SceneNode** pNearestNode) const;

    std::unique_ptr<SceneNode> m_root;
};
